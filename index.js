const express = require("express");
const app = express();
const body_parser = require("body-parser");
const cors = require("cors");
const db=require("./database-config/db");
const routes=require("./routes");

app.use(cors());
app.use(body_parser.json());
app.use("/",routes);

app.listen(4000, (err, data) => {
    if (err) console.log("Error in starting server ");
    else console.log("Server started at 4000");
})
