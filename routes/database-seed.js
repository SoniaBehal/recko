const db_seed=require("../controllers/database-seed");
module.exports=[
    {
        method: 'GET',
        path: '/seed/db',
        handlers: [db_seed.seedDb]
    }
]