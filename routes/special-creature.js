const specialCreature=require("../controllers/special-creature");
module.exports=[
    {
        method: 'GET',
        path: '/families/:universe',
        handlers: [specialCreature.listFamilies]
    },
    {
        method: 'GET',
        path: '/check/powers',
        handlers: [specialCreature.checkPower]
    },
    {
        method: 'GET',
        path: '/balance/families',
        handlers: [specialCreature.balanceFamilies]
    }
]