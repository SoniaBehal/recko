let seeders = {
    'universe': require("./universe"),
    'family': require("./family"),
    'person': require("./person")
}

module.exports = {
    async seed() {
            for (let seeder in seeders) {
                await seeders[seeder].run()
            }
    }
}