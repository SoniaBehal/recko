const family = require("../seed-data/family");
const db = require("../db");
module.exports = {
    async run() {

        let createDb = `CREATE TABLE IF NOT EXISTS family (name VARCHAR(30),
                        id VARCHAR(10) NOT NULL PRIMARY KEY, 
                        attributes VARCHAR (20))`;
        let seedDb = "INSERT INTO family (name, id, attributes) VALUES ?"

        db.query(createDb, function (err, result) {
            if (err) throw err;
            console.log("Family table created");
        })
        db.query(seedDb, [family], function (err, result) {
            if (err) throw err;
            console.log("Family db seeded");
        })
    }
}
