const universe = require("../seed-data/universe");
const db = require("../db");
module.exports = {
    async run() {
        
            let createDb = "CREATE TABLE IF NOT EXISTS universe (name VARCHAR(30),id VARCHAR(10) NOT NULL PRIMARY KEY)";
            let seedDb = "INSERT INTO universe (name, id) VALUES ?"

            db.query(createDb, function (err, result) {
                if (err) throw err;
                console.log("Universe table created");
            })
            db.query(seedDb, [universe], function (err, result) {
                if (err) throw err;
                console.log("Universe db seeded");
            })
        
    }
}
