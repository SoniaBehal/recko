const person = require("../seed-data/person");
const db = require("../db");
module.exports = {
    async run() {

        let createDb = `CREATE TABLE IF NOT EXISTS person (name VARCHAR(30),
                        id INT AUTO_INCREMENT NOT NULL PRIMARY KEY, 
                        power INT , 
                        uni_id VARCHAR(10) , 
                        fm_id VARCHAR(10),
                        FOREIGN KEY(uni_id) REFERENCES universe(id),
                        FOREIGN KEY(fm_id) REFERENCES family(id))`;
        let seedDb = "INSERT INTO person (name, power , uni_id ,fm_id) VALUES ?"

        db.query(createDb, function (err, result) {
            if (err) throw err;
            console.log("Person table created");
        })
        db.query(seedDb, [person], function (err, result) {
            if (err) throw err;
            console.log("Person db seeded");
        })
    }
}
