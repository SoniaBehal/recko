const db = require("../database-config/db");
module.exports = {
    async listFamilies(req, res) {
        let familyListquery = `SELECT * FROM family WHERE id IN 
                ( SELECT DISTINCT fm_id FROM person WHERE uni_id="${req.params.universe}")`
        new Promise((resolve, reject) => {
            db.query(familyListquery, (err, result) => {
                if (err) reject(err);
                resolve(result);
            })
        })
            .then(result => {
                res.send({ status: true, families: result })
            })
            .catch(err => {
                res.send({ status: false, error: err })
            })




    },
    async checkPower(req, res) {
        let familyListquery = `SELECT CONCAT(fm_id,"-",uni_id) AS grp,SUM(power) as totalpower
         FROM person GROUP BY grp`;

        let powers = new Promise((resolve, reject) => {
            db.query(familyListquery, (err, result) => {
                if (err) reject(err);
                resolve(result);
            })
        })
        powers.then(p => {
            let familyUniverses = `SELECT CONCAT(f.id,"-",u.id) AS grp,f.id FROM family f ,
             universe u ORDER BY f.id`;
            let familyGrps = new Promise((resolve, reject) => {
                db.query(familyUniverses, (err, result) => {
                    if (err) reject(err);
                    resolve(result);
                })
            })
            familyGrps.then(fg => {
                let hasSamePower = true;
                let id, val;

                for (let f in fg) {

                    let index = p.findIndex(pf => pf.grp == fg[f].grp);
                    if (index == -1) {
                        hasSamePower = false;
                        break;
                    }
                    else if (fg[f].id != id) {
                        id = fg[f].id, val = p[index].totalpower;
                    }
                    else if (val != p[index].totalpower) {
                        hasSamePower = false;
                        break;
                    }
                }
                if (hasSamePower) message = "All families have same powers in all universes";
                else message = "Some families don't have same powers in all universes";
                res.send({ status: true, message: message })
            })

        })
            .catch(err => {
                res.send({ status: false, error: err })
            })

    },
    async balanceFamilies(req, res) {
        let familyListquery = `SELECT CONCAT(fm_id,"-",uni_id) AS grp,SUM(power) as totalpower
         FROM person GROUP BY grp ORDER BY totalpower DESC`;

        let powers = new Promise((resolve, reject) => {
            db.query(familyListquery, (err, result) => {
                if (err) reject(err);
                resolve(result);
            })
        })
        powers.then(p => {
            let familyUniverses = `SELECT CONCAT(f.id,"-",u.id) AS grp,f.id FROM family f ,
             universe u ORDER BY f.id`;
            let familyGrps = new Promise((resolve, reject) => {
                db.query(familyUniverses, (err, result) => {
                    if (err) reject(err);
                    resolve(result);
                })
            })
            familyGrps.then(fg => {
                let id, val;
                let familiesToBalance = [];
                for (let f in fg) {

                    let index = p.findIndex(pf => pf.grp == fg[f].grp);
                    if (index == -1) {
                        res.send({
                            status: false,
                            message: "Inappropiate data available. Can't add persons to universe"
                        })
                    }
                    else if (fg[f].id != id) {
                        id = fg[f].id, val = p[index].totalpower;
                    }
                    else if (val != p[index].totalpower) {
                        let ind = familiesToBalance.indexOf(id);
                        if (ind == -1) familiesToBalance.push(id);
                    }
                }
                if (familiesToBalance.length) {
                    let param = familiesToBalance.reduce((arr,cr)=>{
                        if(!arr) return '"'+cr+'"'
                        return arr+',"'+cr+'"';
                    },"")
                    let balanceQuery = `UPDATE person SET power=0 WHERE fm_id IN (${param})`;
                    let balance = new Promise((resolve, reject) => {
                        db.query(balanceQuery, (err, result) => {
                            if (err) reject(err);
                            resolve(result);
                        })
                    })
                    balance.then(b => {
                        res.send({ status: true, familiesToBalance: familiesToBalance, message: "Families are balanced" })
                    })
                }
                else res.send({ status: true, familiesToBalance: familiesToBalance, message: "Families are already balanced" })

            })

        })
            .catch(err => {
                res.send({ status: false, error: err })
            })
    }

}