const db = require("../database-config/db");
const seeder = require("../database-config/seeders")
module.exports = {
    async seedDb(req, res) {
        try {
            db.query("CREATE DATABASE IF NOT EXISTS parallel_universe", async (err, result) => {
                if (err) throw err;
                console.log("Database created");
                await seeder.seed();
                res.send({ status: true, message: "Seed completed successfully" })
            })
        }
        catch (err) {
            res.send({ status: false, error: err })
        }
    }
}