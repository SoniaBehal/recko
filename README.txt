Steps to follow: (Serving application)
1. Create the user and database by using MySql workbench with query "CREATE DATABASE IF NOT EXISTS parallel_universe"
2. In the .env file change the USER and PASSWORD according to the data set in previous steps.
3. Now rum npm i
4. Now start the application with "nodemon".


(Database Seeds)
1. With the help of third parties like "Postman", hit the route "localhost:4000/seed/db"
2. All the dummy data for test will be seeded.

(Routes for special creature from 4th dimension)
1. List families in a particular universe  --   localhost:4000/families/id_of_universe
2. Check if families with same identifiers have same power in all universes  --  localhost:4000/check/powers
3. Find unbalanced families and balance them    --  localhost:4000/balance/families

After step 3 data will be modified and when u will hit route 2 again all the powers will be same.




